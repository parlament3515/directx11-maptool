using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor.AssetImporters;
using UnityEditor;

public class SceneExporter : MonoBehaviour
{
    public GameObject exportParent;
    List<byte> datas = new List<byte> ();
    int currentIndex = -1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Export()
    {
        currentIndex = -1;
        ExportRecursive(exportParent.transform, -1);

        System.IO.File.WriteAllBytes( SceneManager.GetActiveScene().name + ".sceneinfo"
                                    , datas.ToArray());
    }

    private void ExportRecursive(Transform t, int parentIndex)
    {
        int childCount = t.childCount;

        for (int i = 0; i < childCount; i++)
        {
            string data = ""; 

            Transform child = t.GetChild(i);

            currentIndex++;

            int childIndex = currentIndex;

            data += childIndex.ToString() + "^";
            data += parentIndex.ToString();
            data += '|';
            data += child.gameObject.name + "^";
            data += child.gameObject.tag;
            data += '|';
            data += "Transform^";
            data += child.localPosition;
            data += '^';
            data += child.localEulerAngles;
            data += '^';
            data += child.localScale;
            data += '|';

            MeshFilter mesh = child.GetComponent<MeshFilter>();

            if (mesh != null)
            {
                data += "ModelRenderer^";
                string meshPath = AssetDatabase.GetAssetPath(mesh.sharedMesh);
                meshPath = meshPath.Replace("Assets/", "");
                meshPath = meshPath.Replace("/", "\\");
                data += meshPath;
                data += '|';
            }

            Light light = child.GetComponent<Light>();

            if (light != null)
            {
                switch(light.type)
                {
                    case LightType.Directional:
                        data += "DirectionalLight^";
                        data += light.color.ToString();
                        break;

                    case LightType.Point:
                        data += "PointLight^";
                        data += light.color.ToString() + "^";
                        data += light.range.ToString();
                        break;

                    case LightType.Spot:
                        data += "SpotLight^";
                        data += light.color.ToString() + "^";
                        data += light.range.ToString() + "^";
                        data += light.innerSpotAngle.ToString() + "^";
                        data += light.spotAngle.ToString();
                        break;
                }

                data += '|';
            }

            Rigidbody rb = child.GetComponent<Rigidbody>();

            if (rb != null)
            {
                data += "Rigidbody^";
                data += "Dynamic^";
                data += rb.mass.ToString() + "^";
                data += rb.drag.ToString() + "^";
                data += rb.angularDrag.ToString() + "^";
                data += rb.useGravity.ToString() + "^";
                data += rb.isKinematic.ToString() + "^";
                data += rb.constraints.ToString() + "^";
                data += rb.freezeRotation.ToString() + "^";
                data += '|';
            }

            Collider[] colliders = child.GetComponentsInChildren<Collider>(); 

            if (colliders.Length > 0)
            {
                if (rb == null)
                {
                    data += "Rigidbody^";
                    data += "Static";
                    data += '|';
                }
            }

            for (int j = 0; j < colliders.Length; j++)
            {
                if (colliders[j] is BoxCollider)
                {
                    BoxCollider collider = (BoxCollider)colliders[j];

                    data += "BoxCollider^";
                    data += collider.isTrigger.ToString() + "^";
                    data += collider.center.x.ToString() + ",";
                    data += collider.center.y.ToString() + ",";
                    data += collider.center.z.ToString() + "^";
                    data += collider.size.x.ToString() + ",";
                    data += collider.size.y.ToString() + ",";
                    data += collider.size.z.ToString() + "^";
                }
                else if (colliders[j] is SphereCollider)
                {
                    SphereCollider collider = (SphereCollider)colliders[j];

                    data += "SphereCollider^";
                    data += collider.isTrigger.ToString() + "^";
                    data += collider.center.ToString() + "^";
                    data += collider.radius.ToString() + "^";
                }
                else if (colliders[j] is CapsuleCollider)
                {
                    CapsuleCollider collider = (CapsuleCollider)colliders[j];

                    data += "CapsuleCollider^";
                    data += collider.isTrigger.ToString() + "^";
                    data += collider.center.ToString() + "^";
                    data += collider.radius.ToString() + "^";
                    data += collider.height.ToString() + "^";
                    data += collider.direction.ToString() + "^";
                }
                else if (colliders[j] is MeshCollider)
                {
                    MeshCollider collider = (MeshCollider)colliders[j];

                    data += "MeshCollider^";
                    data += collider.isTrigger.ToString() + "^";
                }

                PhysicMaterial mat = colliders[j].material;

                if (mat != null)
                {
                    data += "material^";
                    data += mat.staticFriction.ToString() + "^";
                    data += mat.dynamicFriction.ToString() + "^";
                    data += mat.bounciness.ToString() + "^";
                }
                else
                {
                    mat = colliders[j].sharedMaterial;

                    if (mat != null)
                    {
                        data += "material^";
                        data += mat.staticFriction.ToString() + "^";
                        data += mat.dynamicFriction.ToString() + "^";
                        data += mat.bounciness.ToString() + "^";
                    }
                }

                data += '|';
            }

            data = data + ";";
            datas.AddRange(System.Text.Encoding.UTF8.GetBytes(data));

            ExportRecursive(child, childIndex);
        }
    }
}
